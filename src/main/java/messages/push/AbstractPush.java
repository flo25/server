package messages.push;

import model.Incident;

public abstract class AbstractPush {

    public Incident incident;
    public String title;
    public String body;

    protected AbstractPush(Incident pIncident){incident = pIncident;}
}
