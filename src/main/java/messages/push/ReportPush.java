package messages.push;

import model.Incident;
import org.apache.commons.text.StringSubstitutor;

import java.util.HashMap;
import java.util.Map;

public class ReportPush extends TokenPush {
    private static final String TITEL ="${titel}";
    private static final String BODY ="Es wurde folgende Störung gemeldet: \n ${titel} \n Wo: ${location}";

    public ReportPush(Incident pIncident, String pToken) {
        super(pIncident, pToken);
        title=TITEL;
        body=BODY;
        Map<String, String> params = new HashMap<>();
        params.put("titel", pIncident.getTitel());
        params.put("location", pIncident.getLocation());
        title = StringSubstitutor.replace(title, params);
        body = StringSubstitutor.replace(body, params);
    }
}
