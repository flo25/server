package messages.push;

import com.google.firebase.messaging.*;

import java.time.temporal.ChronoUnit;

public class PushHelper {

    public static void sendPushTopic(String title, String body, String topic) {

        Message message = Message.builder().setAndroidConfig(AndroidConfig.builder()
                .setTtl(ChronoUnit.WEEKS.getDuration().toMillis())
                .setPriority(AndroidConfig.Priority.NORMAL)
                .setNotification(AndroidNotification.builder()
                        //todo evtl noch icon eibinden ob warnung oder entwarnung
                        .setTitle(title)
                        .setBody(body)
                        .build())
                .build())
                .setTopic(topic)
                .build();

        try {
            FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }

    }

    public static void sendPushToken(String title, String body, String token) {

        Message message = Message.builder().setAndroidConfig(AndroidConfig.builder()
                .setTtl(ChronoUnit.WEEKS.getDuration().toMillis())
                .setPriority(AndroidConfig.Priority.NORMAL)
                .setNotification(AndroidNotification.builder()
                        .setTitle(title)
                        .setBody(body)
                        .build())
                .build())
                .setToken(token)
                .build();
        try {
            String response = FirebaseMessaging.getInstance().send(message);
            System.out.println(">>>>" + response);

        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }

    public static void sendPushTopic(TopicPush tPush) {
        sendPushTopic(tPush.title, tPush.body, tPush.topic);
    }

    public static void sendPushToken(TokenPush tPush) {
        sendPushToken(tPush.title, tPush.body, tPush.token);
    }
}
