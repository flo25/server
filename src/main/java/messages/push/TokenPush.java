package messages.push;

import model.Incident;

public class TokenPush extends AbstractPush {
    public String token;
    public TokenPush(Incident pIncident, String pToken) {
        super(pIncident);
        this.token =pToken;
    }
}
