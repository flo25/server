package messages.push;

import model.Incident;

public class TopicPush extends AbstractPush {
    public String topic;

    public TopicPush(Incident pIncident, String pTopic) {
        super(pIncident);
        this.topic = pTopic;
    }
}
