package database;

import model.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static model.Role.ADMIN;
import static model.Role.EMPLOYEE;

public class SQLDB {

    private static final SQLDB INSTANCE = new SQLDB();

    public static SQLDB get() {
        return INSTANCE;
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final SimpleDateFormat time = new SimpleDateFormat("HH:mm");
    Connection connection = IMServer.con;
    Statement stmt = null;

    public void createTableUser() throws SQLException {
        stmt = connection.createStatement();
        stmt.executeUpdate("if object_id('" + DBStrings.USER + "','U') is not null drop table " + DBStrings.USER);
        String query = "create table " + DBStrings.USER +
                " (" +
                DBStrings.UID + " integer, " +
                DBStrings.U_NAME + " varchar(30), " +
                DBStrings.U_NOTIFICATION + " integer, " +
                DBStrings.U_ROLE + " varchar(15), " +
                DBStrings.U_LOCATION + " varchar(20), " +
                DBStrings.U_TOKEN + " varchar(300));";
        stmt.execute(query);
        stmt.close();
    }

    public void createTableIncident() throws SQLException {
        stmt = connection.createStatement();
        stmt.executeUpdate("if object_id('" + DBStrings.INCIDENT + "','U') is not null drop table " + DBStrings.INCIDENT);
        String query = "create table " + DBStrings.INCIDENT + " (" +
                DBStrings.I_ID + " integer, " +
                DBStrings.I_TITEL + " varchar(30), " +
                DBStrings.I_DESCRIPTION + " varchar(300), " +
                DBStrings.I_LOCATION + " varchar(20), " +
                DBStrings.I_STARTTIME + " varchar(20), " +
                DBStrings.I_ENDTIME + " varchar(20), " +
                DBStrings.I_DATE + " varchar(20), " +
                DBStrings.I_SENDER + " varchar(30), " +
                DBStrings.I_ISWARNING + " integer, " +
                DBStrings.I_ADMINID + " integer, " +
                DBStrings.I_SENDTO + " integer, " +
                DBStrings.I_SENDSTATE + " integer);";
        stmt.executeUpdate(query);
        stmt.close();
    }

    public void createTableList() throws SQLException {
        stmt = connection.createStatement();
        stmt.executeUpdate("if object_id('" + DBStrings.L_LISTS + "','U') is not null drop table " + DBStrings.L_LISTS);
        String query = "create table " + DBStrings.L_LISTS + " (" +
                DBStrings.L_INCIDENTS + " text, " +
                DBStrings.L_LOCATION + " text);";
        stmt.executeUpdate(query);
        stmt.close();
    }

    //LLIST

    //gleich bei Serverstart aufrufen damit initalisiert werden kann
    public void createList(LLists list) {
        try {
            stmt = connection.createStatement();
            String query = " INSERT INTO " + DBStrings.L_LISTS +
                    " (" + DBStrings.L_INCIDENTS + ", " +
                    DBStrings.L_LOCATION + ") " +
                    "VALUES ('" + list2String(list.incidents) + "', '" +
                    list2String(list.locations) + "' );";
            stmt.executeUpdate(query);
            stmt.close();
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("createList failed!!");
        }
    }

    private String list2String(List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s).append("~");
        }
        return sb.toString();
    }

    private List<String> string2List(String string) {
        String[] a = string.split("~");
        return new ArrayList<>(Arrays.asList(a));
    }

    public void updateList(List<String> incidents, List<String> locations) {
        try (Statement stmt = connection.createStatement()) {
            StringBuilder query = new StringBuilder();
            query.append("UPDATE " + DBStrings.L_LISTS);

            List<String> setClauses = new ArrayList<>();
            setClauses.add(DBStrings.L_INCIDENTS + " = '" + list2String(incidents) + "'");
            setClauses.add(DBStrings.L_LOCATION + " = " + list2String(locations) + "'");
            if (!setClauses.isEmpty()) {
                query.append(" SET ");
                for (String s : setClauses) {
                    if (setClauses.indexOf(s) > 0) {
                        query.append(" , ");
                    }
                    query.append(s);
                }
            }

            stmt.executeUpdate(query.toString());
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("updateList failed!!");
        }
    }

    public LLists getList() {
        LLists list = null;
        try (Statement stmt = connection.createStatement()) {
            String query = "SELECT * FROM " + DBStrings.L_LISTS;

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                list = new LLists(string2List(rs.getString(DBStrings.L_INCIDENTS)), string2List(rs.getString(DBStrings.L_LOCATION)));
            }
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("getList failed!!");
        }
        return list;
    }


    //USER
    public void createUser(int id, String name, String role, boolean notification, String location, String token) {
        try (Statement stmt = connection.createStatement()) {
            String query = " INSERT INTO " + DBStrings.USER +
                    " ( " + DBStrings.UID + ", " +
                    DBStrings.U_NAME + ", " +
                    DBStrings.U_ROLE + ", " +
                    DBStrings.U_NOTIFICATION + ", " +
                    DBStrings.U_LOCATION + ", " +
                    DBStrings.U_TOKEN + ") " +
                    "VALUES ( " + id + ", '" +
                    name + "', '" +
                    role + "', " +
                    (notification ? 1 : 0) + ", '" +
                    location + "', '" +
                    token + "');";
            stmt.executeUpdate(query);
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("createUser failed!!");
        }
    }

    public void updateUser(int id, String name, String role, boolean notification, String location) {
        try (Statement stmt = connection.createStatement()) {
            StringBuilder query = new StringBuilder();
            query.append("UPDATE " + DBStrings.USER);

            List<String> setClauses = new ArrayList<>();
            if (name != null && !name.isEmpty()) {
                setClauses.add(DBStrings.U_NAME + " = '" + name + "'");
            }
            if (role != null && !role.isEmpty()) {
                setClauses.add(DBStrings.U_ROLE + " = '" + role + "'");
            }
            if (location != null && !location.isEmpty()) {
                setClauses.add(DBStrings.U_LOCATION + " = '" + location + "'");
            }
            setClauses.add(DBStrings.U_NOTIFICATION + " = " + (notification ? 1 : 0));
            if (!setClauses.isEmpty()) {
                query.append(" SET ");
                for (String s : setClauses) {
                    if (setClauses.indexOf(s) > 0) {
                        query.append(" , ");
                    }
                    query.append(s);
                }
            }
            query.append("WHERE " + DBStrings.UID + "=").append(id);

            stmt.executeUpdate(query.toString());
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("updateUser failed!!");
        }
    }

    public User getUser(int uid) {
        User user = null;
        try (Statement stmt = connection.createStatement()) {
            String query = "SELECT * FROM " + DBStrings.USER + " WHERE " + DBStrings.UID + "=" + uid + ";";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                user = new User(rs.getInt(DBStrings.UID), rs.getString(DBStrings.U_NAME), getRoleByName(rs.getString(DBStrings.U_ROLE)),
                        (rs.getInt(DBStrings.U_NOTIFICATION) == 1), rs.getString(DBStrings.U_LOCATION), rs.getString(DBStrings.U_TOKEN));
            }
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("getUser failed!!");
        }
        return user;
    }

    private Role getRoleByName(String name) {
        if (name.equals(ADMIN.name)) {
            return ADMIN;
        } else {
            return EMPLOYEE;
        }
    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            String query = "SELECT * FROM " + DBStrings.USER;

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                users.add(new User(rs.getInt(DBStrings.UID), rs.getString(DBStrings.U_NAME), getRoleByName(rs.getString(DBStrings.U_ROLE)),
                        (rs.getInt(DBStrings.U_NOTIFICATION) == 1), rs.getString(DBStrings.U_LOCATION), rs.getString(DBStrings.U_TOKEN)));
            }
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("getUser failed!!");
        }
        return users;
    }

    public List<User> getUsers(String name, String role, boolean notification, String location) {
        List<User> users = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {

            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM " + DBStrings.USER);

            List<String> whereClauses = new ArrayList<>();
            if (name != null && !name.isEmpty()) {
                whereClauses.add(DBStrings.U_NAME + " = '" + name + "'");
            }
            if (role != null && !role.isEmpty()) {
                whereClauses.add(DBStrings.U_ROLE + " = '" + role + "'");
            }
            if (location != null && !location.isEmpty()) {
                whereClauses.add(DBStrings.U_LOCATION + " = '" + location + "'");
            }
//            whereClauses.add(DBStrings.U_NOTIFICATION + " = " + (notification ? 1 : 0));
            if (!whereClauses.isEmpty()) {
                query.append(" WHERE ");
                for (String s : whereClauses) {
                    if (whereClauses.indexOf(s) > 0) {
                        query.append(" AND ");
                    }
                    query.append(s);
                }
            }

            ResultSet rs = stmt.executeQuery(query.toString());
            while (rs.next()) {
                users.add(new User(rs.getInt(DBStrings.UID), rs.getString(DBStrings.U_NAME), getRoleByName(rs.getString(DBStrings.U_ROLE)),
                        (rs.getInt(DBStrings.U_NOTIFICATION) == 1), rs.getString(DBStrings.U_LOCATION), rs.getString(DBStrings.U_TOKEN)));
            }
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("getUsers failed!!");
        }
        return users;
    }

    public void deleteUser(int id) {
        try (Statement stmt = connection.createStatement()) {
            String where = " WHERE " + DBStrings.UID + "=" + id;
            String query = "DELETE from " + DBStrings.USER + where;

            stmt.executeUpdate(query);
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("deleteUser failed!!");
        }
    }

    //INCIDENT
    public void createIncident(int id, String titel, String description, String location, String startTime, String endtime,
                               String idate, String sender, boolean isWarning, int adminId, int sendTo, int sendState) {
        try (Statement stmt = connection.createStatement()) {
            String query = " INSERT INTO " + DBStrings.INCIDENT +
                    " ( " + DBStrings.I_ID + ", " +
                    DBStrings.I_TITEL + ", " +
                    DBStrings.I_DESCRIPTION + ", " +
                    DBStrings.I_LOCATION + ", " +
                    DBStrings.I_STARTTIME + ", " +
                    DBStrings.I_ENDTIME + ", " +
                    DBStrings.I_DATE + ", " +
                    DBStrings.I_SENDER + ", " +
                    DBStrings.I_ISWARNING + ", " +
                    DBStrings.I_ADMINID + ", " +
                    DBStrings.I_SENDTO + ", " +
                    DBStrings.I_SENDSTATE + ") " +
                    "VALUES ( " + id + ", '" +
                    titel + "', '" +
                    description + "', '" +
                    location + "', '" +
                    startTime + "', '" +
                    endtime + "', '" +
                    idate + "', '" +
                    sender + "', " +
                    (isWarning ? 1 : 0) + ", " +
                    adminId + ", " +
                    sendTo + ", " +
                    sendState + ");";
            System.out.println(query);
            stmt.executeUpdate(query);
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("createIncident failed!!");
        }
    }

    public void updateIncident(int id, String titel, String description,
                               String location, String startTime, String endtime, String date,
                               String sender, boolean isWarning, int adminId, int sendTo,
                               int sendState) {
        try (Statement stmt = connection.createStatement()) {

            StringBuilder query = new StringBuilder();
            query.append("UPDATE " + DBStrings.INCIDENT);

            List<String> whereClauses = new ArrayList<>();
            if (titel != null && !titel.isEmpty()) {
                whereClauses.add(DBStrings.I_TITEL + " = '" + titel + "'");
            }
            if (description != null && !description.isEmpty()) {
                whereClauses.add(DBStrings.I_DESCRIPTION + " = '" + description + "'");
            }
            if (location != null && !location.isEmpty()) {
                whereClauses.add(DBStrings.I_LOCATION + " = '" + location + "'");
            }
            if (startTime != null) {
                whereClauses.add(DBStrings.I_STARTTIME + " = '" + time.format(startTime) + "'");
            }
            if (endtime != null) {
                whereClauses.add(DBStrings.I_ENDTIME + " = '" + time.format(endtime) + "'");
            }
            if (date != null) {
                whereClauses.add(DBStrings.I_DATE + " = '" + dateFormat.format(date) + "'");
            }
            if (sender != null && !sender.isEmpty()) {
                whereClauses.add(DBStrings.I_SENDER + " = '" + sender + "'");
            }
            if (adminId != -1 && adminId != 0) {
                whereClauses.add(DBStrings.I_ADMINID + " = " + adminId);
            }
            if (sendTo != -1) {
                whereClauses.add(DBStrings.I_SENDTO + " = " + sendTo);
            }
            if (sendState != -1) {
                whereClauses.add(DBStrings.I_SENDSTATE + " = " + sendState);
            }
            whereClauses.add(DBStrings.I_ISWARNING + " = " + (isWarning ? 1 : 0));

            if (!whereClauses.isEmpty()) {
                query.append(" SET ");
                for (String s : whereClauses) {
                    if (whereClauses.indexOf(s) > 0) {
                        query.append(" , ");
                    }
                    query.append(s);
                }
            }
            query.append(" WHERE " + DBStrings.I_ID + "=").append(id);


            stmt.executeUpdate(query.toString());
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("updateIncidents failed!!");
        }
    }

    public Incident getIncident(int iId) {
        Incident incident = null;
        try (Statement stmt = connection.createStatement()) {
            String query = "SELECT * FROM " + DBStrings.INCIDENT + " WHERE " + DBStrings.I_ID + "=" + iId + ";";

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                incident = new Incident(rs.getInt(DBStrings.I_ID), rs.getString(DBStrings.I_TITEL), rs.getString(DBStrings.I_DESCRIPTION), rs.getString(DBStrings.I_LOCATION),
                        rs.getString(DBStrings.I_STARTTIME), rs.getString(DBStrings.I_ENDTIME), rs.getString(DBStrings.I_DATE), rs.getString(DBStrings.I_SENDER),
                        (rs.getInt(DBStrings.I_ISWARNING) == 1), rs.getInt(DBStrings.I_ADMINID), rs.getInt(DBStrings.I_SENDTO), rs.getInt(DBStrings.I_SENDSTATE));
            }
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("getIncident failed!!");
        }
        return incident;
    }

    public List<Incident> getIncidents(String titel, String description, String location, String startTime,
                                       String endtime, String date, String startDate, String endDate, String sender, boolean isWarning, int adminId) {
        List<Incident> incidents = new ArrayList<>();

        try (Statement stmt = connection.createStatement()) {

            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM " + DBStrings.INCIDENT);

            List<String> whereClauses = new ArrayList<>();
            if (titel != null && !titel.isEmpty()) {
                whereClauses.add(DBStrings.I_TITEL + " = '" + titel + "'");
            }
            if (description != null && !description.isEmpty()) {
                whereClauses.add(DBStrings.I_DESCRIPTION + " = '" + description + "'");
            }
            if (location != null && !location.isEmpty()) {
                whereClauses.add(DBStrings.I_LOCATION + " = '" + location + "'");
            }
            if (startTime != null) {
                whereClauses.add(DBStrings.I_STARTTIME + " = '" + startTime + "'");
            }
            if (endtime != null) {
                whereClauses.add(DBStrings.I_ENDTIME + " = '" + endtime + "'");
            }
            if (date != null) {
                whereClauses.add(DBStrings.I_DATE + " = '" + date + "'");
            }
            //Filter
            if (startDate != null) {
                whereClauses.add(DBStrings.I_DATE + " >= '" + startDate + "'");
            }
            if (endDate != null) {
                whereClauses.add(DBStrings.I_DATE + " <= '" + endDate + "'");
            }
            if (sender != null && !sender.isEmpty()) {
                whereClauses.add(DBStrings.I_SENDER + " = '" + sender + "'");
            }
            if (adminId != -1 && adminId != 0) {
                whereClauses.add(DBStrings.I_ADMINID + " = " + adminId);
            }
//            whereClauses.add(DBStrings.I_ISWARNING + " = " + (isWarning ? 1 : 0));

            if (!whereClauses.isEmpty()) {
                query.append(" WHERE ");
                for (String s : whereClauses) {
                    if (whereClauses.indexOf(s) > 0) {
                        query.append(" AND ");
                    }
                    query.append(s);
                }
            }
            ResultSet rs = stmt.executeQuery(query.toString());
            while (rs.next()) {
                incidents.add(new Incident(rs.getInt(DBStrings.I_ID), rs.getString(DBStrings.I_TITEL), rs.getString(DBStrings.I_DESCRIPTION), rs.getString(DBStrings.I_LOCATION),
                        rs.getString(DBStrings.I_STARTTIME), rs.getString(DBStrings.I_ENDTIME), rs.getString(DBStrings.I_DATE), rs.getString(DBStrings.I_SENDER),
                        (rs.getInt(DBStrings.I_ISWARNING) == 1), rs.getInt(DBStrings.I_ADMINID), rs.getInt(DBStrings.I_SENDTO), rs.getInt(DBStrings.I_SENDSTATE)));
                System.out.println(rs.getInt(DBStrings.I_ADMINID));
            }
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("getIncidents failed!!");
        }
        return incidents;
    }

    public void deleteIncident(int id) {
        try (Statement stmt = connection.createStatement()) {
            String where = " WHERE " + DBStrings.I_ID + "=" + id;
            String query = "DELETE from " + DBStrings.INCIDENT + where;

            stmt.executeUpdate(query);
            connection.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("deleteIncident failed!!");
        }
    }
}