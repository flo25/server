package database;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import messages.push.CancelPush;
import messages.push.InfoPush;
import messages.push.PushHelper;
import messages.push.ReportPush;
import model.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Path("INCIDENT_MANAGEMENT")
public class DBHelper {

    private static final int NONE = 0;
    private static final int ADMIN = 1;
    private static final int WARNING = 2;
    private static final int ALL_CLEAR = 3;

    private static final int ALL = 0;
    private static final int INGOLSTADT = 1;
    private static final int GARCHING = 2;

    ExecutorService threadPool = Executors.newCachedThreadPool((runnable) -> {
        Thread t = new Thread(runnable);
        t.setDaemon(true);
        return t;
    });


    //LIST
    @Path("/list/update")
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    public void updateList(@QueryParam(DBStrings.L_INCIDENTS) List<String> incidents,
                           @QueryParam(DBStrings.L_LOCATION) List<String> locations) {
        SQLDB.get().updateList(incidents, locations);
    }

    @Path("/list/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public LLists getList() {
        return SQLDB.get().getList();
    }


    //USER
    @Path("/users/create")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createUser(@QueryParam(DBStrings.UID) int id, @QueryParam(DBStrings.U_NAME) String name,
                           @QueryParam(DBStrings.U_ROLE) String role,
                           @QueryParam(DBStrings.U_NOTIFICATION) boolean notification,
                           @QueryParam(DBStrings.U_LOCATION) String location,
                           @QueryParam(DBStrings.U_TOKEN) String token) {
        SQLDB.get().createUser(id, name, role, notification, location, token);
    }

    @Path("/users/update/{id}")
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    public void updateUser(@PathParam("id") int id, @QueryParam(DBStrings.U_NAME) String name,
                           @QueryParam(DBStrings.U_ROLE) String role,
                           @QueryParam(DBStrings.U_NOTIFICATION) boolean notification,
                           @QueryParam(DBStrings.U_LOCATION) String location) {
        SQLDB.get().updateUser(id, name, role, notification, location);
    }

    @Path("/users/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@QueryParam(DBStrings.UID) int uid) {
        return SQLDB.get().getUser(uid);
    }

    @Path("/users/gets")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUsers(@QueryParam(DBStrings.U_NAME) String name, @QueryParam(DBStrings.U_ROLE) String role,
                               @QueryParam(DBStrings.U_NOTIFICATION) boolean notification, @QueryParam(DBStrings.U_LOCATION) String location) {
        return SQLDB.get().getUsers(name, role, notification, location);
    }

    @Path("/users/delete/{id}")
    @DELETE
    public void deleteUser(@PathParam("id") int id) {
        SQLDB.get().deleteUser(id);
    }

    //INCIDENT
    @Path("/incident/create")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createIncident(@QueryParam(DBStrings.I_ID) int id,
                               @QueryParam(DBStrings.I_TITEL) String titel,
                               @QueryParam(DBStrings.I_DESCRIPTION) String description,
                               @QueryParam(DBStrings.I_LOCATION) String location,
                               @QueryParam(DBStrings.I_STARTTIME) String startTime,
                               @QueryParam(DBStrings.I_ENDTIME) String endtime,
                               @QueryParam(DBStrings.I_DATE) String date,
                               @QueryParam(DBStrings.I_SENDER) String sender,
                               @QueryParam(DBStrings.I_ISWARNING) boolean isWarning,
                               @QueryParam(DBStrings.I_ADMINID) int adminId,
                               @QueryParam(DBStrings.I_SENDTO) int sendTo,
                               @QueryParam(DBStrings.I_SENDSTATE) int sendState) {
        System.out.println("create Incident api method");
        SQLDB.get().createIncident(id, titel, description, location, startTime, endtime, date, sender, isWarning, adminId, sendTo, sendState);
        System.out.println("created");
    }

    @Path("/incident/update/{id}")
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    public void updateIncident(@PathParam("id") int id,
                               @QueryParam(DBStrings.I_TITEL) String titel,
                               @QueryParam(DBStrings.I_DESCRIPTION) String description,
                               @QueryParam(DBStrings.I_LOCATION) String location,
                               @QueryParam(DBStrings.I_STARTTIME) String startTime,
                               @QueryParam(DBStrings.I_ENDTIME) String endtime,
                               @QueryParam(DBStrings.I_DATE) String date,
                               @QueryParam(DBStrings.I_SENDER) String sender,
                               @QueryParam(DBStrings.I_ISWARNING) boolean isWarning,
                               @QueryParam(DBStrings.I_ADMINID) int adminId,
                               @QueryParam(DBStrings.I_SENDTO) int sendTo,
                               @QueryParam(DBStrings.I_SENDSTATE) int sendState) {
        SQLDB.get().updateIncident(id, titel, description, location, startTime, endtime, date, sender, isWarning, adminId, sendTo, sendState);

        threadPool.execute(() -> {
            Incident incident = SQLDB.get().getIncident(id);
            List<User> users = SQLDB.get().getUsers();
            List<String> recipients = new ArrayList<>();
            switch (sendState) {
                case ADMIN:
                    for (User u : users) {
                        if (u.isNotification() && u.getRole() == Role.ADMIN) {
                            recipients.add(u.getToken());
                            PushHelper.sendPushToken(new ReportPush(incident, u.getToken()));
                        }
                    }
                    break;
                case WARNING:
                    switch (sendTo) {
                        case ALL:
                            for (User u : users) {
                                if (u.isNotification()) {
                                    recipients.add(u.getToken());
                                }
                            }
                            sendInfoNotification(incident, recipients, String.valueOf(id));
                            break;
                        case INGOLSTADT:
                            for (User u : users) {
                                if (u.isNotification() && u.getLocation().equals(Location.INGOLSTADT.name)) {
                                    recipients.add(u.getToken());
                                }
                            }
                            sendInfoNotification(incident, recipients, String.valueOf(id));
                            break;
                        case GARCHING:
                            for (User u : users) {
                                if (u.isNotification() && u.getLocation().equals(Location.GARCHING.name)) {
                                    recipients.add(u.getToken());
                                }
                            }
                            sendInfoNotification(incident, recipients, String.valueOf(id));
                            break;
                    }
                    break;
                case ALL_CLEAR:
                    switch (sendTo) {
                        case ALL:
                            for (User u : users) {
                                if (u.isNotification()) {
                                    recipients.add(u.getToken());
                                }
                            }
                            sendCancelNotification(incident, recipients, String.valueOf(id));
                            break;
                        case INGOLSTADT:
                            for (User u : users) {
                                if (u.isNotification() && u.getLocation().equals(Location.INGOLSTADT.name)) {
                                    recipients.add(u.getToken());
                                }
                            }
                            sendCancelNotification(incident, recipients, String.valueOf(id));
                            break;
                        case GARCHING:
                            for (User u : users) {
                                if (u.isNotification() && u.getLocation().equals(Location.GARCHING.name)) {
                                    recipients.add(u.getToken());
                                }
                            }
                            sendCancelNotification(incident, recipients, String.valueOf(id));
                            break;
                    }
                    break;
            }
        });
    }

    private void sendInfoNotification(Incident incident, List<String> recipients, String topic) {
        subscribeToTopic(recipients, topic);
        PushHelper.sendPushTopic(new InfoPush(incident, topic));
    }

    private void sendCancelNotification(Incident incident, List<String> recipients, String topic) {
        PushHelper.sendPushTopic(new CancelPush(incident, topic));
        unSubscribeToTopic(recipients, topic);
    }

    private void subscribeToTopic(List<String> users, String topic) {
        try {
            if (users != null) {
                FirebaseMessaging.getInstance().subscribeToTopic(users, topic);
            }
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }

    private void unSubscribeToTopic(List<String> users, String topic) {
        try {
            if (users != null) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(users, topic);
            }
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
    }

    @Path("/incident/get")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Incident getIncident(@QueryParam(DBStrings.I_ID) int iId) {
        return SQLDB.get().getIncident(iId);
    }

    @Path("/incident/gets")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Incident> getIncidents(@QueryParam(DBStrings.I_TITEL) String titel,
                                       @QueryParam(DBStrings.I_DESCRIPTION) String description,
                                       @QueryParam(DBStrings.I_LOCATION) String location,
                                       @QueryParam(DBStrings.I_STARTTIME) String startTime,
                                       @QueryParam(DBStrings.I_ENDTIME) String endtime,
                                       @QueryParam(DBStrings.I_DATE) String date,
                                       @QueryParam(DBStrings.I_STARTDATE) String startDate,
                                       @QueryParam(DBStrings.I_ENDDATE) String endDate,
                                       @QueryParam(DBStrings.I_SENDER) String sender,
                                       @QueryParam(DBStrings.I_ISWARNING) boolean isWarning,
                                       @QueryParam(DBStrings.I_ADMINID) int adminId) {
        return SQLDB.get().getIncidents(titel, description, location, startTime, endtime, date, startDate, endDate, sender, isWarning, adminId);
    }

    @Path("/incident/delete/{id}")
    @DELETE
    public void deleteIncident(@PathParam("id") int id) {
        SQLDB.get().deleteIncident(id);
    }
}
