package database;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import model.DBStrings;
import model.LLists;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IMServer {

    static int port = 7896;
    static int dbPort = 1433;
    static String ip = "localhost";
    static String hostname;
    public static Connection con;
    static final Logger logger = Logger.getLogger("Serverlog");
    private static boolean firstRun = false;
    private static final String jdbcDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    public static void main(String[] args) throws Exception {
        logger.setLevel(Level.INFO);
        logger.info("Server starting");
        try {
            ip = Inet4Address.getLocalHost().getHostAddress();
            hostname = Inet4Address.getLocalHost().getCanonicalHostName();
            logger.info("IP: " + ip + "\nHostname: " + hostname);
        } catch (UnknownHostException pE) {
            pE.printStackTrace();
        }
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        HandlerList handlers = new HandlerList();
        handlers.addHandler(context);

        org.eclipse.jetty.server.Server jettyServer = new org.eclipse.jetty.server.Server(port);
        jettyServer.setHandler(handlers);


        ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitOrder(0);

        servletHolder.setInitParameter(
                "jersey.config.server.provider.classnames",
                DBHelper.class.getCanonicalName());

        logger.info("Starting SQL database on port: " + dbPort);
        File file = new File("D:\\AbschlussProjekt Doant IT\\firstrun.txt");
        if (!file.exists()) {
            logger.info("First run");
            firstRun = true;
            file.createNewFile();
        }
        try {
            Class.forName(jdbcDriver);
            Class.forName(jdbcDriver).getDeclaredConstructor().newInstance();
            logger.info("JDBC driver loaded");
            con = DriverManager.getConnection("jdbc:sqlserver://localhost\\INCIDENT_MANAGER:" +
                    dbPort + ";databaseName=INCIDENT_MANAGEMENT;user=dbapi;password=20Test20;encrypt=false");
            if (firstRun) {
                SQLDB.get().createTableUser();
                SQLDB.get().createTableIncident();
                SQLDB.get().createTableList();
                SQLDB.get().createList(new LLists(DBStrings.INCIDENTLIST, DBStrings.LOCATIONLIST));
                logger.info("First run. Table created");
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        logger.info("Opened database successfully");

        FileInputStream serviceAccount = null;
        try {
            serviceAccount = new FileInputStream("incident-management-a5f0f-firebase-adminsdk-yl7zc-6afc771866.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FirebaseApp.initializeApp(options);

        try {
            logger.info("Starting Jetty-Server on port " + port);
            logger.info("Base URL: " + getBaseUrl());
            jettyServer.start();
            //System.err.println(jettyServer.dump());
            jettyServer.join();

        } catch (Exception ex) {
            logger.warning("Shutting down");
            jettyServer.stop();
            jettyServer.destroy();
            con.close();
            throw new RuntimeException(ex);
        }
    }

    public static String getBaseUrl() {
        return "http://" + ip + ":" + port;
    }
}
