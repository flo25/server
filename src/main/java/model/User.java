package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {

    private final int uID;
    private final String name;
    private final Role role;
    private final boolean notification;
    private final String location;
    private final String token;

    @JsonCreator
    public User(@JsonProperty(DBStrings.UID) int uID, @JsonProperty(DBStrings.U_NAME) String name, @JsonProperty(DBStrings.U_ROLE) Role role,
                @JsonProperty(DBStrings.U_NOTIFICATION) boolean notification, @JsonProperty(DBStrings.U_LOCATION) String location, @JsonProperty(DBStrings.U_TOKEN) String token) {
        this.uID = uID;
        this.name = name;
        this.role = role;
        this.notification = notification;
        this.location = location;
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public Role getRole() {
        return role;
    }

    public boolean isNotification() {
        return notification;
    }

    public String getLocation() {
        return location;
    }

    public String getToken() {
        return token;
    }

    public int getuID() {
        return uID;
    }
}
