package model;

public enum Location {
    ALL("All"),
    INGOLSTADT("Ingolstadt"),
    GARCHING("Garching");

    public final String name;

    Location(String pName) {
        this.name = pName;
    }

    public Location getLocationByName(String name) {
        if (name.equals(Location.ALL.name)) {
            return Location.ALL;
        } else if (name.equals(Location.INGOLSTADT.name)) {
            return Location.INGOLSTADT;
        } else {
            return Location.GARCHING;
        }
    }

}
