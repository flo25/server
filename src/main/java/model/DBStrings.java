package model;

import java.util.Arrays;
import java.util.List;

public final class DBStrings {
    public static final String USER = "users";
    public static final String UID = "uID";
    public static final String U_ROLE = "role";
    public static final String U_LOCATION = "location";
    public static final String U_NAME = "name";
    public static final String U_NOTIFICATION = "notification";
    public static final String U_TOKEN = "token";
    public static final String INCIDENT = "incident";
    public static final String I_ID = "incidentId";
    public static final String I_TITEL = "titel";
    public static final String I_DESCRIPTION = "description";
    public static final String I_LOCATION = "location";
    public static final String I_STARTTIME = "startTime";
    public static final String I_ENDTIME = "endTime";
    public static final String I_DATE = "date";
    public static final String I_SENDER = "sender";
    public static final String I_ISWARNING = "isWarning";
    public static final String I_ADMINID = "adminID";
    public static final String I_SENDTO = "sendTo";
    public static final String I_SENDSTATE = "sendState";
    public static final String L_LISTS = "lists";
    public static final String L_INCIDENTS = "incidentList";
    public static final String L_LOCATION = "locationList";
    //Filter
    public static final String I_STARTDATE = "startDate";
    public static final String I_ENDDATE = "endDate";

    //constants
    public static final List<String> INCIDENTLIST = Arrays.asList("Netzwerkausfall", "Emailserverausfall", "Telefonausfall", "Stromausfall");
    public static final List<String> LOCATIONLIST = Arrays.asList("Ingolstadt", "Garching", "Home Office");
}
