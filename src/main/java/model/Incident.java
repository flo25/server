package model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Incident implements Serializable {

    private final int incidentID;
    private final String titel;
    private final String description;
    private final String location;
    private final String startTime;
    private final String endTime;
    private final String date;
    private final String sender;
    private final boolean isWarning;
    private final int adminId;
    private final int sendTo;
    private final int sendState;

    @JsonCreator
    public Incident(@JsonProperty(DBStrings.I_ID) int incidentID, @JsonProperty(DBStrings.I_TITEL) String titel, @JsonProperty(DBStrings.I_DESCRIPTION) String description,
                    @JsonProperty(DBStrings.I_LOCATION) String location, @JsonProperty(DBStrings.I_STARTTIME) String startTime, @JsonProperty(DBStrings.I_ENDTIME) String endTime,
                    @JsonProperty(DBStrings.I_DATE) String date, @JsonProperty(DBStrings.I_SENDER) String sender, @JsonProperty(DBStrings.I_ISWARNING) boolean isWarning,
                    @JsonProperty(DBStrings.I_ADMINID) int adminId, @JsonProperty(DBStrings.I_SENDTO) int sendTo, @JsonProperty(DBStrings.I_SENDSTATE) int sendState) {
        this.incidentID = incidentID;
        this.titel = titel;
        this.description = description;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
        this.date = date;
        this.sender = sender;
        this.isWarning = isWarning;
        this.adminId = adminId;
        this.sendTo = sendTo;
        this.sendState = sendState;
    }

    public String getTitel() {
        return titel;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getDate() {
        return date;
    }

    public String getSender() {
        return sender;
    }

    public boolean isWarning() {
        return isWarning;
    }

    public int getIncidentID() {
        return incidentID;
    }

    public int getAdminId() {
        return adminId;
    }

    public int getSendTo() {
        return sendTo;
    }

    public int getSendState() {
        return sendState;
    }
}
