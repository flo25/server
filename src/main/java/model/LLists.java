package model;

import java.util.List;

public class LLists {

    public List<String> incidents;
    public List<String> locations;

    public LLists(List<String> incidents, List<String> locations) {
        this.incidents = incidents;
        this.locations = locations;
    }
}
