package model;

public enum Role {
    ADMIN("Administrator"),
    EMPLOYEE("Mitarbeiter");

    public final String name;

    Role(String pName) {
        this.name = pName;
    }

    public Role getRoleByName(String name) {
        if (name.equals(ADMIN.name)) {
            return ADMIN;
        } else {
            return EMPLOYEE;
        }
    }

}

